// 1. Initialize the game state
const gameBoard = document.getElementById('game-board');
const gameStatus = document.getElementById('game-status');
const resetButton = document.getElementById('reset-button');
let currentPlayer = 'X';
let board = ['', '', '', '', '', '', '', '', ''];
let gameActive = true;

// 2. Handle player clicks
gameBoard.addEventListener('click', function(event) {
  const clickedCell = event.target;
  const clickedCellIndex = Array.from(gameBoard.children).indexOf(clickedCell);

  // If the cell has already been clicked or the game is inactive, ignore the click
  if (board[clickedCellIndex] !== '' || !gameActive) {
    return;
  }

  // Place the symbol and update the board state
  board[clickedCellIndex] = currentPlayer;
  clickedCell.innerHTML = `<span class="fade-in">${currentPlayer}</span>`;

  // 3. Check for a win or a tie
  checkForWinner();

  // 4. Toggle the player's turn
  currentPlayer = currentPlayer === 'X' ? 'O' : 'X';

  // 5. Display the current player's turn
  updateGameStatus();
});

// 6. Implement a reset button
resetButton.addEventListener('click', resetGame);

// Function to update the game status message
function updateGameStatus() {
  gameStatus.textContent = `Player ${currentPlayer}'s turn`;
}

// Function to check for a winner or a tie
function checkForWinner() {
  // Define winning combinations
  const winningCombinations = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];

  let roundWon = false;

  for (let i = 0; i < winningCombinations.length; i++) {
    const [a, b, c] = winningCombinations[i];
    if (board[a] && board[a] === board[b] && board[a] === board[c]) {
      roundWon = true;
      break;
    }
  }

  if (roundWon) {
    gameStatus.textContent = `Player ${currentPlayer} has won!`;
    gameActive = false;
    return;
  }

  // Check for a tie
  const roundTie = !board.includes('');
  if (roundTie) {
    gameStatus.textContent = 'Game is a tie!';
    gameActive = false;
    return;
  }
}

// Function to reset the game
function resetGame() {
  currentPlayer = 'X';
  board = ['', '', '', '', '', '', '', '', ''];
  gameActive = true;
  updateGameStatus();

  Array.from(gameBoard.children).forEach(cell => {
    cell.innerHTML = '';
  });
}

// Call updateGameStatus to set the initial message
updateGameStatus();
